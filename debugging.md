# Debugging

Debugging distributed application is always a complex task. Here is a set of
advices and tips to help you in this journey.

You might get the following error which, I admit, is not very helpful:
"Libomptarget fatal error 1: failure of target construct while offloading is
mandatory". In this case, you should enable the debug message of the OmpCluster
runtime using "export LIBOMPTARGET_DEBUG=1". Then, you should see many messages
in the execution log including some more interesting errors like "Target library
loading error: /tmp/tmpfile_zSK0IW: undefined symbol: lapack_constants" which
basically means lapack_constants is used in the target region and should be
declare as such, using the declare target pragmas.
