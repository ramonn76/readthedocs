First example
-------------

To better understand this model, we make an example with block-based
matrix multiplication. Given two matrices, the task to multiply them.

If there are two input matrices A and B of size N and the output matrix
C. The multiplication of matrices A and B is given through:

.. code:: sh

    void MatMul(int &A, int &B, int &C) {
      for (int i = 0; i < N; ++i){
        for (int j = 0; j < N ; ++j) {
          C[i][j] = 0;
          for (int k = 0; k < N ; ++k) {
            C[i][j] += BlockA[i][k] * BlockB[k][j];
          }
        }
      }
    }

.. figure:: img/matrix-multiplication-good.gif
   :alt: matrix-multiplication-good

   matrix-multiplication-good
It is possible to use a block partitioned matrix product that involves
only algebra on submatrices of the factors. The partitioning of the
factors is not arbitrary, however, and requires conformable partitions
between two matrices *A* and *B* such that all submatrix products that
will be used are defined.

The following figure shows the block-based matrix multiplication.

.. figure:: img/block_multiplication.png
   :alt: block\_multiplication

   block\_multiplication
In this case, the code would be.

.. code:: sh

    void BlockMatMul(BlockMatrix &A, BlockMatrix &B, BlockMatrix &C) {
      // Go through all the blocks of the matrix.
      for (int i = 0; i < N / BS; ++i)
      {
        for (int j = 0; j < N / BS; ++j)
        {
          float *BlockC = C.GetBlock(i, j);
          for (int k = 0; k < N / BS; ++k) {
            float *BlockA = A.GetBlock(i, k);
            float *BlockB = B.GetBlock(k,j);
            // Go through the block.
            for(int ii = 0; ii < BS; ii++)
              for(int jj = 0; jj < BS; jj++) {
                for(int kk = 0; kk < BS; ++kk)
                  BlockC[ii + jj * BS] += BlockA[ii + kk * BS] * BlockB[kk + jj * BS];
              }
          }
        }
      }
    }

The BlockMatrix class is only used as a utility wrapper to split the
whole matrix into blocks so as to benefit from data locality (each block
is contained by a different array).

We can parallelize the code sending each block to a different node and
in the node, the multiplication of each block is performed.

.. code:: sh

    void BlockMatMul(BlockMatrix &A, BlockMatrix &B, BlockMatrix &C) {
      #pragma omp parallel
      #pragma omp single
      for (int i = 0; i < N / BS; ++i)
        for (int j = 0; j < N / BS; ++j) {
          float *BlockC = C.GetBlock(i, j);
          for (int k = 0; k < N / BS; ++k) {
            float *BlockA = A.GetBlock(i, k);
            float *BlockB = B.GetBlock(k,j);
            #pragma omp target depend(in: BlockA[0], BlockB[0]) \
                               depend(inout: BlockC[0]) \
                               map(to: BlockA[:BS*BS], BlockB[:BS*BS]) \
                               map(tofrom: BlockC[:BS*BS]) nowait
            #pragma omp parallel for
            for(int ii = 0; ii < BS; ii++)
              for(int jj = 0; jj < BS; jj++) {
                for(int kk = 0; kk < BS; ++kk)
                  BlockC[ii + jj * BS] += BlockA[ii + kk * BS] * BlockB[kk + jj * BS];
              }
          }
        }
    }

As we carry out the multiplication of each block in the node, we have to
send the block of matrix A, the block of matrix B as input
(``map(to: BlockA[:BS*BS], BlockB[:BS*BS])``), and the block of matrix C
as output and input (``map(tofrom: BlockC[:BS*BS])``). The
multiplication process depends on input blocks A and B
(``depend(in: BlockA[0], BlockB[0])``) and block C as output
(``depend(inout: BlockC[0])``).
