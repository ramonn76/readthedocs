.. OmpCluster documentation master file, created by
   sphinx-quickstart on Tue Nov 17 10:20:07 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Ftask's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   programming
   usage
   container
   debugging
   example
   omptracing



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
