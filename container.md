# Container

To easily experiment with the OmpCluster tools, we provide a set of  
[docker images](docker-hub) containing a pre-compiled Clang/LLVM with all
necessary OpenMP and MPI libraries to compile and run any OmpCluster programs.
Different configurations are available: Ubuntu or CentOS based images including
CUDA or not with MPICH or OpenMPI. Choose the docker image tag according to your
favorite configuration or `latest` to use the default configuration.

You can execute your applications with the OmpCluster runtime on any computer
using docker and the following command:
```
docker run -v /path/to/my_program/:/root/my_program -it ompcluster/runtime:latest /bin/bash
cd /root/my_program/
```
This flags `-v` is used to share a folder between the operating system of the
host and the container. You can get more information on how to use Docker in the
official [Get Started][docker] guide).

You can also use Singularity, using the following commands for example:
```
singularity pull docker://ompcluster/runtime:latest
singularity shell ./runtime_latest.sif
cd /path/to/my_program/
```
See [here][singularity] for more information about Singularity.


## Existing images and configurations

The container images that we provide follows this naming convention:
`ompcluster/<image_name>:<tag>`.

Several images are available on our docker-hub repository, here is a tentative
to list them:
 - `hpcbase`: the base image for all other containers. It contains the MPI
 implementation, CUDA, Mellanox drivers, etc.
 - `runtime`: this image contains the pre-built Clang and OmpCluster
 runtime based on the stable releases.
 - `runtime-dev`:  this image contains the pre-built Clang and OmpCluster
 runtime based on the Git repository. This version should be considered as
 unstable and should not be used in production.
 - Application-specific images (`àwave-dev`, `beso-dev`, `plasma-dev`, etc):
 Those images are based on the runtime image but contains additional libraries
 and tools required to develop some applications.


[docker]: https://docs.docker.com/get-started/
[singularity]: https://sylabs.io/guides/3.2/user-guide/
[docker-hub]: https://hub.docker.com/r/ompcluster/
