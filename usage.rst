Compile and run programs
------------------------

Compiling OpenMP code for OmpCluster requires to use a specific OpenMP
target ``x86_64-pc-linux-gnu`` which indicates to the compiler to
compile the OpenMP target code region for a device. For example, the
*mat-mul* example can be compiled using the following command:

.. code:: sh

    clang -fopenmp -fopenmp-targets=x86_64-pc-linux-gnu mat-mul.cpp -o mat-mul

Then, you can run the newly created program but, contrary to classical
OpenMP programs, programs need to be executed using ``mpirun`` or
``mpiexec`` tools to use the OmpCluster distributed task runtime just as
any other MPI program:

.. code:: sh

    mpirun -np 3 ./mat-mul

In this case, the runtime will automatically create 3 MPI processes (one
head and two workers): the head process will offload OpenMP target
regions to be executed on the workers following the currently
implemented scheduling strategy.

The runtime also supports the offloading to remote MPI processes
(located on other computers or containers), those can be configured
using the ``-host`` or ``-hostfile`` flags of ``mpirun``. However, just
as any MPI programs, the user needs to copy the binary on all
computers/containers before executing it (using ``pdcp`` command or NFS
directory).

As you can see OpenMP offloading debug messages can quickly become too
verbose. They are enabled by default but you can disable them by
executing the following command before running the program:

.. code:: sh

    export LIBOMPTARGET_DEBUG=0
