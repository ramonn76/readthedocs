# OmpTracing

This tool can be used to trace the execution of an OpenMP (or OmpCluster)
application.

## Usage

To enable OmpTracing you just need to set the OMP_TOOL_LIBRARIES environment
variable to the path of the OmpTracing library.

```
export OMP_TOOL_LIBRARIES=/path/to/libomptracing.so
./your-own-omp-program
```

If you use one of the container image provided by OmpCluster, OmpTracing library
is already provided in `/opt/omptracing/lib/libomptracing.so`, otherwise you can
compile it from its [repository](https://gitlab.com/ompcluster/omptracing/).

After the execution of the program ends, OmpTracing should have produced two
files: a JSON file containing the tracing of the execution and a DOT file
describing the task graph produced by the OpenMP runtime.

## Tracing

You can use Chrome browser as an graphical interface to see the tracing of the
program execution. Just open Chrome (or Chromium), enter `chrome://tracing` in
the address field. A new interface should appear where you can load the
omptracing.json file just as presented in the image below.

![tracing](img/tracing.png)

*  **Parallel Region:** it marks the begin and the end of parallel regions. Each
region specifies an ID and the number of threads requested.

![parallel_region](img/parallel_region.png)

*  **Thread Region:** it marks the begin and the end of thread regions. The thread
type is specified. To get more information, see
[ompt_thread_t](https://www.openmp.org/spec-html/5.0/openmpsu185.html#x233-11730004.4.4.10)
data in OMPT documentation.

![thread_region](img/thread_region.png)

*  **Tasks:** it marks the begin and the end of tasks. The tasks are divided in
levels. A parent task is a task that created another one, and tasks of level
zero have no parent tasks. The level information is described in the label
section, as well as the parent task id and the task type. To get more
information, see
[ompt_task_flag_t](https://www.openmp.org/spec-html/5.0/openmpsu185.html#x233-11980004.4.4.18)
data in ompt documentation.

![task_region](img/task_region.png)

*  **Work Region:** it marks the begin and the end of work regions like loop,
taskloop, sections, workshare, single regions and distribute regions and gave
information about it. To get more information, see
[ompt_callback_work_t](https://www.openmp.org/spec-html/5.0/openmpsu187.html#x236-12830004.5.2.5)
data in OMPT documentation.

*  **Implicit Task Region:** it marks the begin and the end of implicit task
regions and gave your number of threads/teams. To get more information see
[ompt_callback_implicit_task_t](https://www.openmp.org/spec-html/5.0/openmpsu187.html#x236-13190004.5.2.11)
data in OMPT documentation.

*  **Master Region:** it marks the begin and the end of master regions. To get
more informations see
[ompt_callback_master_t](https://www.openmp.org/spec-html/5.0/openmpsu187.html#x236-13250004.5.2.12)
data in OMPT documentation.

*  **Sync Region:** it marks the begin and the end of sync regions like barrier
implicit/explicit, taskwait, taskgroup and reduction. To get more informations
see
[ompt_callback_sync_region_t](https://www.openmp.org/spec-html/5.0/openmpsu187.html#x236-13310004.5.2.13)
data in OMPT documentation.

## Task Graph

OmpTracing also produce a dot file representing the task-dependency graph of the
OpenMP program. You can produce a PDF file to visualize it using the following
command:

```
dot -Tpdf graph.gv > graph.pdf
```

## Configuration

```
{
  "callbacks": [
    "task",
    "thread",
    "parallel",
    "work",
    "implicit_task",
    "master",
    "sync_region"
  ],
  "max_tasks":128,
  "graph_reduction":"yes",
  "graph_time":"yes"
}
```

You can choose which information will tracing by providing a configuration json
file as presented in the code above. A update example is available
[here](https://gitlab.com/ompcluster/omptracing/-/blob/master/config-example.json)
in OmpTracing repository. Just add or remove some callbacks to the json array to
add or remove the tracing of those information. Then you can pass the file path
to the OMPTRACING_CONFIG_PATH enviroment variable or simply place this file in
build folder. If you don't pass a configuration file the tracing will use the
default values.
